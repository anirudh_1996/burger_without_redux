import React, {Component} from 'react';
import Aux from '../AuxContainer/Aux';
import  './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component{
    state={
        showSideDrawer:false
    }

    sideDrawerClosedHandler=()=>{
        this.setState({showSideDrawer:false});
}
sideDrawerOpenHandler=()=>{
    this.setState({showSideDrawer:true});
}

render(){
    return(
        <Aux>
        <Toolbar toggleOn={this.sideDrawerOpenHandler}/>
        <SideDrawer open={this.state.showSideDrawer} closed={this.sideDrawerClosedHandler}/>
        <main className="Content">
            {this.props.children}
        </main>  
    </Aux>
    )
}
}
export default Layout;
