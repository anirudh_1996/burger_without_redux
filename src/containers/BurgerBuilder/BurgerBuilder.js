import React, {Component} from 'react';
import Aux from '../../hoc/AuxContainer/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControl/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';


const INGREDIENT_PRICES={
    salad:0.5,
    cheese:0.7,
    meat:1.0,
    bacon:0.9
}
class BurgerBulder extends Component{
    // constructor(props){
    //     super(props);
    //     this.state={...}
    // }
state={
    ingredient:null,
    totalPrice:0.3,
    purchasable:false,
    purchasing:false,
    loading:false,
    error:false
}
componentDidMount(){
    axios.get('/ingredients.json')
    .then(res=>{
        this.setState({ingredient:res.data})
    })
    .catch(err=>{
        this.setState({error:true})
    }
        )
}
addIngredientHandler=(type)=>{
        const oldCount =this.state.ingredient[type];
        const updatedCount=oldCount+1;
        const updatedIngredients={
            ...this.state.ingredient
        }
        updatedIngredients[type]=updatedCount;

        const priceAddition=INGREDIENT_PRICES[type];
        const oldPrice=this.state.totalPrice;
        const newPrice=priceAddition+oldPrice;      
        this.setState({totalPrice:newPrice, ingredient:updatedIngredients});
        this.updatePurchaseState(updatedIngredients);
    }
removeIngredientHandler=(type)=>{
    const oldCount =this.state.ingredient[type];
    const updatedCount=oldCount-1;
    const updatedIngredients={
        ...this.state.ingredient
    }
    if(updatedCount<0){
        return;
    } else{
    updatedIngredients[type]=updatedCount;
    }
    const priceAddition=INGREDIENT_PRICES[type];
    const oldPrice=this.state.totalPrice;
    var newPrice=oldPrice-priceAddition;      
    this.setState({totalPrice:newPrice, ingredient:updatedIngredients});
    this.updatePurchaseState(updatedIngredients);
}
updatePurchaseState(updatedIngredients){
    const sum=Object.values(updatedIngredients).reduce((su,el)=>{
        return su + el;
    }, 0);
    
    this.setState({purchasable: sum>0});
}
purchaseHandler(){
    this.setState({purchasing:true});
}
purchaseHandlerCancel=()=>{
    this.setState({purchasing:false});
}
purchaseHandlerContinue=()=>{
    const queryParams=[];
    for(let i in this.state.ingredient){
        queryParams.push(encodeURIComponent(i)+'='+encodeURIComponent(this.state.ingredient[i]))
    }
    queryParams.push(encodeURIComponent('price')+'='+encodeURIComponent(this.state.totalPrice));
    const queryString=queryParams.join('&');
    this.props.history.push({
        pathname:'/checkout',
        search:'?'+queryString
    });
}

    render(){
        
        let orderSummmary=null;
        
        let burger=this.state.error ? <p>Ingredients Can't be loaded</p> : <Spinner/>;
        if(this.state.ingredient){
            burger=(
                <Aux>
                     <Burger ingredients={this.state.ingredient}/>
        <BuildControls ordered={()=>this.purchaseHandler()} purchasable={!this.state.purchasable} totalPrice={this.state.totalPrice} removeIngredientHandler={this.removeIngredientHandler} addIngredientHandler={this.addIngredientHandler}/>
                </Aux>
            );
            orderSummmary=<OrderSummary clicked={this.purchaseHandlerContinue} ingredients={this.state.ingredient} tprice={this.state.totalPrice}/>;
        }
        if(this.state.loading){
            console.log("test here")
            orderSummmary=<Spinner/> 
        }
        return(
            <Aux>
                <Modal show={this.state.purchasing} clicked={this.purchaseHandlerCancel}>
                    <button onClick={this.purchaseHandlerCancel} style={{float:'right', WebkitBorderRadius:'5px', backgroundColor:'#ccc', border:'1px solid black' }}>x</button>
                    {orderSummmary}
                </Modal>
            {burger}
            </Aux>
        )
    }
}
export default withErrorHandler(BurgerBulder,axios);