import React, {Component} from 'react';
import Button from '../../../components/UI/Button/Button';
import './ContactData.css';
import Spinner from '../../../components/UI/Spinner/Spinner';
import axios from '../../../axios-orders';
import Input from '../../../components/UI/Input/Input';


class ContactData extends Component{
    state={
       orderForm:{ 
        name:{
        elementType:'input',
        elementConfig:{
            type:'text',
            placeholder:'Your Name'
        },
        value:''
        },
        email:{
            elementType:'email',
            elementConfig:{
                type:'text',
                placeholder:'your email'
            },
            value:''
            },
        street:{
            elementType:'input',
            elementConfig:{
                type:'text',
                placeholder:'street'
            },
            value:''
            },
        postalCode:{
            elementType:'input',
            elementConfig:{
                type:'text',
                placeholder:'POSTAL code'
            },
            value:''
            },
            country:{
                elementType:'input',
                elementConfig:{
                    type:'text',
                    placeholder:'country'
                },
                value:''
                },
        deliveryMethod:{
            elementType:'select',
            elementConfig:{
               options:[{value:'fatsest',displayValue:'Fastest'},
               {value:'cheapest',displayValue:'Cheapest'}]
            },
            value:''
            }
    },
        loading:false
    }
    orderHandler=(event)=>{
        event.preventDefault();
      this.setState({loading:true})  
    const order={
        ingredients:this.props.ingredients,
        price:this.props.price,
        customer:{
            name:this.state.orderForm.name.value,
                street:this.state.orderForm.street.value,
                zipCode:this.state.orderForm.postalCode.value,
                Country:this.state.orderForm.country.value,
            email:this.state.orderForm.email.value,
            deliveryMethod:this.state.orderForm.deliveryMethod.value
        }
       
    }
    axios.post('/orders.json',order)
    .then(res=>{
        console.log(res);
        this.setState({loading:false})
      this.props.history.push('/');
    })
    .catch(err=>{
        console.log(err,"Something went wrong!");
        this.setState({loading:false})
    })
    }
    inputChangedHandler=(event,inputIdentifier)=>{
        let UpdatedForm={
            ...this.state.orderForm
        };
        let UpdatedFormIn={
            ...this.state.orderForm[inputIdentifier]
        };
        
        UpdatedFormIn.value=event.target.value;
        UpdatedForm[inputIdentifier]=UpdatedFormIn;
        //console.log(UpdatedForm,inputIdentifier);
        this.setState({orderForm:UpdatedForm});
        
    }
    render(){
        const formElemArray=[];
        for(let key in this.state.orderForm){
            formElemArray.push({
                id:key,
                config:this.state.orderForm[key]
            })
        }
        const formElem=formElemArray.map((ord)=>{
            return <Input key={ord.id} elementType={ord.config.elementType} 
            elementConfig={ord.config.elementConfig}  
            value={ord.config.value}
            changed={(event)=>{this.inputChangedHandler(event,ord.id)}}/>
            });
        let form=(
            <form onSubmit={this.orderHandler} className='ContactData'>
                {formElem}
                    
                    <Button btnType="Success">Order</Button>
                </form>
        );
        if(this.state.loading){
            form=<Spinner/>
        } 
        return(
            <div className="ContactData">
                <h4>Enter Your Contact Data</h4>
                
            {form}
               
            </div>
        )
    }
}

export default ContactData;