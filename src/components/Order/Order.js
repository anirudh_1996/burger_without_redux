import React from 'react';

import './Order.css';
const Order=(props)=>{
    let ingredient=[];
    for(let key in props.ingredients){
        ingredient.push(key+":"+props.ingredients[key]);
    }
    let tingredient=ingredient.map(ing=>{
    return <span style={{textTransform:'capitalize', display:'inline-block',margin:'0 8px', border:'1px solid #ccc',padding:'5px'}}>{ing}</span>
    })
    return (
        
        <div className='Order'>
            <strong>Ingredients: </strong>
            <p>{tingredient}</p>
    <p>Price: <strong>{Number.parseFloat(props.price).toFixed(2)}</strong></p>
            </div>
    );
}

export default Order;