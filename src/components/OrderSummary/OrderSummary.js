import React, { Component } from "react";
import Aux from '../../hoc/AuxContainer/Aux'
import Button from '../UI/Button/Button';

class OrderSummary extends Component{
    componentDidUpdate(){
        console.log("order summary will update");
    }

 render(){
    const ingredientSummary=Object.keys(this.props.ingredients)
    .map(igkey=>{
    return (<li key={Math.random()}><span style={{textTransform:'capitalize'}} >{igkey}</span>: {this.props.ingredients[igkey]}</li>);
    })
     return(
        <Aux>
            <h3>Your Order</h3>
            <p>A delicious burger with the following ingredients</p>
        <ul>
            {ingredientSummary}
        </ul>
    <p><strong>Total Price: {this.props.tprice}</strong></p>
        <p>Continue to checkout?</p>
        <Button clicked={this.props.clicked}>Continue</Button>
        </Aux>
     )
 }
} 

export default OrderSummary;