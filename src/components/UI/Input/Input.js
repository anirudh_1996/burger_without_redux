import React from 'react';
import './Input.css';

const Input=(props)=>{
    let inputElement=null;
    switch(props.elementType){
        case('input'):
        inputElement=<input 
                        className="Input" {...props.elementConfig} onChange={props.changed} value={props.value} required minLength='5'/>
        break;
        case('textarea'):
        inputElement=<textarea 
                        className="Input" {...props} {...props.elementConfig} value={props.value} onChange={props.changed} required/>
        break;
        case('select'):
        inputElement=<select className="Input"onChange={props.changed} value={props.value} required>
            <option >Select</option>
            {props.elementConfig.options.map(option=>(
                <option key={option.value} value={option.value}>
                    {option.displayValue}
                </option>
            ))}
        </select>
        break;
        default:
            inputElement=<input 
                            className="Input" {...props.elementConfig} onChange={props.changed} value={props.value} required minLength='5'/>
    }
    return(
        <div className="Input">
            <label className="Label"></label>
            {inputElement}
        </div>    
    )
}
export default Input;