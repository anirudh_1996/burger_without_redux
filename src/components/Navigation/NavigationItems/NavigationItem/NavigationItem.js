import React from 'react';
import {NavLink} from 'react-router-dom'

import './NavigationItem.css';

const navigationItem =(props)=>(
    <ul className="NavigationItem">
        <li><NavLink  exact to={props.link} >{props.children}</NavLink></li>
    </ul>
)

export default navigationItem;