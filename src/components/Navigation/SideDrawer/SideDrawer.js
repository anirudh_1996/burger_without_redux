import React from 'react'
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import './SideDrawer.css';
import Aux from '../../../hoc/AuxContainer/Aux';
import BackDrop from '../../UI/BackDrop/BackDrop';

const sideDrawer=(props)=>{
    let attachedClasses=["SideDrawer","Close"];
if(props.open){
    attachedClasses=["SideDrawer","Open"]
}
    return (
        <Aux className="">
          <BackDrop show={props.open} clicked={props.closed}/>  
        <div className={attachedClasses.join(' ')}>
          
        <Logo height="11%"/>
            

        <nav>
            <NavigationItems/>
        </nav>
        </div>
        </Aux>
    );
}

export default sideDrawer;