import React from 'react';
import image from '../../assets/original.png';
import './Logo.css';

const logo =(props)=>{
    return ( 
        <div className="Logo" style={{height:props.height}}> 
            <img src={image} alt="burger pic" />
        </div>
    )
}
 export default logo;