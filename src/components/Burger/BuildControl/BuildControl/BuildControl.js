import React from 'react';

import './BuildControl.css';

const buildControl=(props)=>{
    return (
    <div className="BuildControl">
        <div className="Label">{props.label}</div>
        <button className="Less" onClick={()=>props.props.removeIngredientHandler(props.label.toLowerCase())}>Less</button>
        <button className="More" onClick={()=>props.props.addIngredientHandler(props.label.toLowerCase())}>More</button>
    </div>
    );
};
export default buildControl;