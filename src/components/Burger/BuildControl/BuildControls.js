import React from 'react';
import './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';
const controls=[
    { label:'Salad', type: 'salad'},
    { label:'Bacon', type: 'bacon'},
    { label:'Cheese', type: 'cheese'},
    { label:'Meat', type: 'meat'}
];
const buildControls=(props)=>(
 
<div className="BuildControls">
<p>Current Price: {props.totalPrice} $</p>
{controls.map(ctrl=>(
    <BuildControl key={ctrl.label} label={ctrl.label} props={props}/>
))}
<button onClick={props.ordered} disabled={props.purchasable} className="OrderButton">ORDER NOW</button>
</div>

);

export default buildControls;