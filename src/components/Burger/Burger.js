import React from 'react';
import './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const burger=(props)=>{
    let tingredient=Object.keys(props.ingredients).map((ing)=>{ 
        return [...Array(props.ingredients[ing])].map((_,k)=>{ 
            // console.log(k,Object.values(props.ingredients));
        return <BurgerIngredient key={Math.random()} type={ing}/>

    });
}).reduce((arr,el)=>{
return arr.concat(el)
},[]);
if(tingredient.length===0){
    tingredient=<p>Please start adding ingredient</p>
}

    return (
<div className="Burger">
<BurgerIngredient type="bread-top"/>
    {tingredient}
<BurgerIngredient type="bread-bottom"/>
</div>

    );
}

export default burger;